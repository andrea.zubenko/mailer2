FROM perseusrepo/mailer-v2

COPY project/ project
COPY src/ src
ADD build.sbt .
RUN sbt assembly