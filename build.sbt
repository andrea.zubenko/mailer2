import Dependencies.Runtime._
import Dependencies.Test._
import Dependencies.Build._

ThisBuild / organization := "de.perseus"
ThisBuild / scalaVersion := "2.13.4"

lazy val `mailer` =
  project
    .in(file("."))
    .settings(name := "mailer")
    .settings(commonSettings)
    .settings(dependencies)

lazy val commonSettings = Seq(
  addCompilerPlugin(kindProjector),
  update / evictionWarningOptions := EvictionWarningOptions.empty,
  Compile / console / scalacOptions := {
    (Compile / console / scalacOptions).value
      .filterNot(Scalac.Lint.toSet)
      .filterNot(Scalac.FatalWarnings.toSet) :+ "-Wconf:any:silent"
  },
  Test / console / scalacOptions :=
    (Compile / console / scalacOptions).value
)

lazy val dependencies = Seq(
  libraryDependencies ++= httpServer,
  libraryDependencies ++= database,
  libraryDependencies ++= Seq( // Misc
    pureconfig,
    mandrill
  ),
  libraryDependencies += scalaTest,
  libraryDependencies ++= scalaCheck,
  libraryDependencies += kindProjector
)

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case _                                   => MergeStrategy.first
}
