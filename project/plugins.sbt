ThisBuild / scalaVersion := "2.12.13"
ThisBuild / useSuperShell := false
ThisBuild / autoStartServer := false

update / evictionWarningOptions := EvictionWarningOptions.empty

addSbtPlugin("com.eed3si9n"     % "sbt-assembly"   % "0.15.0")
addSbtPlugin("com.gilt.sbt"     % "sbt-aws-lambda" % "0.7.0")
addSbtPlugin("ch.epfl.scala"    % "sbt-scalafix"   % "0.9.25")
addSbtPlugin("com.timushev.sbt" % "sbt-rewarn"     % "0.1.3")
addSbtPlugin("com.timushev.sbt" % "sbt-updates"    % "0.5.1")
addSbtPlugin("com.typesafe.sbt" % "sbt-git"        % "1.0.0")
addSbtPlugin("io.spray"         % "sbt-revolver"   % "0.9.1")
addSbtPlugin("org.scalameta"    % "sbt-scalafmt"   % "2.4.2")
//addSbtPlugin("org.wartremover" % "sbt-wartremover" % "2.4.13")
