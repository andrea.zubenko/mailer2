import sbt._

object Dependencies {

  object Runtime {
    val tapirVersion              = "0.17.12"
    val httpServer: Seq[ModuleID] = Seq(
      "com.softwaremill.sttp.tapir" %% "tapir-core",
      "com.softwaremill.sttp.tapir" %% "tapir-openapi-docs",
      "com.softwaremill.sttp.tapir" %% "tapir-openapi-circe-yaml",
      "com.softwaremill.sttp.tapir" %% "tapir-json-circe",
      "com.softwaremill.sttp.tapir" %% "tapir-akka-http-server",
      "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-akka-http"
    ).map(_ % tapirVersion)

    val database: Seq[ModuleID]   = Seq(
      "org.mongodb.scala" %% "mongo-scala-driver" % "2.9.0",
      "org.postgresql"     % "postgresql"         % "42.2.19"
    )

    val pureconfig: ModuleID = "com.github.pureconfig"         %% "pureconfig" % "0.14.0"
    val mandrill: ModuleID   = "com.mandrillapp.wrapper.lutung" % "lutung"     % "0.0.8"

  }

  object Test {
    val scalaTest: ModuleID = "org.scalatest" %% "scalatest" % "3.2.3"

    val scalaCheck: Seq[ModuleID] = Seq(
      "com.github.alexarchambault" %% "scalacheck-shapeless_1.14" % "1.2.5",
      "org.scalacheck"             %% "scalacheck"                % "1.15.3",
      "org.scalatestplus"          %% "scalacheck-1-15"           % "3.2.5.0"
    )
  }

  object Build {
    // Note: kindProjector is basically built-in in scala 3, so it will be removed in the future
    val kindProjector: ModuleID   = "org.typelevel"        %% "kind-projector"   % "0.11.3" cross CrossVersion.full
    val organizeImports: ModuleID = "com.github.liancheng" %% "organize-imports" % "0.5.0"
  }
}
